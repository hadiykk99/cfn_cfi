// slideshow function
$(document).ready(function(){	


//subscribe function
$('.mobile-sub').click(function(e) {
	e.preventDefault();
	if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
		var target = $(this.hash);
		target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
		if (target.length) {
			$('html, body').animate({
				scrollTop: target.offset().top+10
			}, 1500);
			return false;
		}
	}
});
//subscribe function

//show more click
$('#btn-showmore').on('click',function(){
	showmore();
});
//show more click

//modal
$('.socmed-item').on('click',function(){
	var name = $(this).find('.socmed-name').html();
	$('#m-name').html(name);

	var uname = $(this).find('.socmed-username').html();
	$('#m-uname').html(uname);

	var type = $(this).find('.socmed-typestyle').attr('src');
	$('.socmed-typestyleModal').attr('src',type);

	var coverimg = $(this).find('.socmed-photostyle').attr('src');
	if(coverimg){
		$('.socmed-photoModal').css('display','flex');
		$('.socmed-photostyleModal').attr('src',coverimg);
	}else{
		$('.socmed-photoModal').css('display','none');
	}

	var egg = $(this).find('.socmed-engage').html();
	$('#m-engage').html(egg);

	var content = $(this).find('.socmed-text').html();
	$('#m-text').html(content);

})
//modal

//socmed menu href
$('.s-mobile').on('click',function(){
	window.location.href = $(this).attr('link');
})
//socmed menu href

//socmed menu arrow
$('#s-right').on('click',function(){
	var oldid = $('.s-show').attr('id').substr(1);
	$('#s'+oldid).removeClass('s-show');
	if(oldid < 6){
		var newid = parseInt(oldid)+1;
		$('#s'+newid).addClass('s-show');
	}else{
		$('#s1').addClass('s-show');
	}
})
$('#s-left').on('click',function(){
	var oldid = $('.s-show').attr('id').substr(1);
	$('#s'+oldid).removeClass('s-show');
	if(oldid > 1){
		var newid = parseInt(oldid)-1;
		$('#s'+newid).addClass('s-show');
	}else{
		$('#s6').addClass('s-show');
	}
})
//socmed menu arrow

//socmed load margin
var width = $(window).width();
socmed_margin(width);
//socmed load margin


//socmed filter toggle
$('.filter-toggle').click(function(){
	var ft = $('.filter-toggle');
	if(ft.attr('data-id') == 's'){
		ft.html('<i class="fa fa-angle-left"></i> Hide Filter');
		$('.filter-radio').css('display','flex');
		$('.filter-radio').removeClass('animated zoomOut');
		$('.filter-radio').addClass('animated zoomIn');
		$('.socmed-formR').css('display','flex');
		$('.socmed-formR').removeClass('animated zoomOut');
		$('.socmed-formR').addClass('animated zoomIn');
		ft.attr('data-id','h');
	}else{
		ft.html('Show Filter <i class="fa fa-angle-right"></i>');
		$('.filter-radio').removeClass('animated zoomIn');
		$('.filter-radio').addClass('animated zoomOut');
		$('.filter-radio').css('display','none');
		$('.socmed-formR').removeClass('animated zoomIn');
		$('.socmed-formR').addClass('animated zoomOut');
		$('.socmed-formR').css('display','none');
		ft.attr('data-id','s');
	}
})
//socmed filter toggle


	$('#next').click(function(){
		var index = $('.active').attr('slide-index');
		if(index == 3){
			$('#3').removeClass('active');
			$('#1').addClass('active');
			$('#1').addClass('animated fadeIn');
		}else{
			$('#'+index).removeClass('active');
			$('#'+(parseInt(index)+1)).addClass('active');
			$('#'+(parseInt(index)+1)).addClass('animated fadeIn');
		}
	});
	$('#prev').click(function(){
		var index = $('.active').attr('slide-index');
		if(index == 1){
			$('#1').removeClass('active');
			$('#3').addClass('active');
			$('#3').addClass('animated fadeIn');
		}else{
			$('#'+index).removeClass('active');
			$('#'+(parseInt(index)-1)).addClass('active');
			$('#'+(parseInt(index)-1)).addClass('animated fadeIn');
		}
	});

	// small device
	$('#next2').click(function(){
		var index = $('.sactive').attr('slide-index');
		if(index == 3){
			$('#S3').removeClass('sactive');
			$('#S1').addClass('sactive');
			$('#S1').addClass('animated fadeIn');
		}else{
			$('#S'+index).removeClass('sactive');
			$('#S'+(parseInt(index)+1)).addClass('sactive');
			$('#S'+(parseInt(index)+1)).addClass('animated fadeIn');
		}
	});
	$('#prev2').click(function(){
		var index = $('.sactive').attr('slide-index');
		if(index == 1){
			$('#S1').removeClass('active');
			$('#S3').addClass('active');
			$('#S3').addClass('animated fadeIn');
		}else{
			$('#S'+index).removeClass('sactive');
			$('#S'+(parseInt(index)-1)).addClass('sactive');
			$('#S'+(parseInt(index)-1)).addClass('animated fadeIn');
		}
	});
	// small device

	// mobile menu
	$('#toggle').click(function(){
		if($('#mobile').css('display') == 'none'){
			$('#mobile').css('display','flex');
		}else{
			$('#mobile').css('display','none');
		}
	})
	// mobile menu

})
// slideshow function

// coinmarketcap API
api();
setInterval(api, 30000);
function api(){	
	$.ajax({
	     url: "https://api.coinmarketcap.com/v1/ticker/bitcoin/",
	     dataType: 'json',
	     success: function(data) {
	     		var price = '$ '+ (Math.round(data[0].price_usd * 100) / 100).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');;
	     		if(data[0].percent_change_24h >= 0){
	        		$('#btc-percent').addClass('up');
	        		var percent = '<i class="fa fa-caret-up"></i> ' + data[0].percent_change_24h + '%';
	        	}else{
	        		$('#btc-percent').addClass('down');
	        		var percent = '<i class="fa fa-caret-down"></i> ' + data[0].percent_change_24h + '%';
	        	}
	        	$('#btc-price').html(price);
	        	$('#btc-percent').html(percent);
	     }
	});
	$.ajax({
	    url: "https://api.coinmarketcap.com/v1/ticker/bitcoin-cash/",
	    dataType: 'json',
	    success: function(data) {
	        var price = '$ '+ (Math.round(data[0].price_usd * 100) / 100).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
	     	if(data[0].percent_change_24h >= 0){
	        	$('#bch-percent').addClass('up');
	        	var percent = '<i class="fa fa-caret-up"></i> ' + data[0].percent_change_24h + '%';
	        }else if(data[0].percent_change_24h < 0){
	        	$('#bch-percent').addClass('down');
	        	var percent = '<i class="fa fa-caret-down"></i> ' + data[0].percent_change_24h + '%';
	        }
	       	$('#bch-price').html(price);
	       	$('#bch-percent').html(percent);
	    }
	});
	$.ajax({
	    url: "https://api.coinmarketcap.com/v1/ticker/ethereum/",
	    dataType: 'json',
	    success: function(data) {
	     	var price = '$ '+ (Math.round(data[0].price_usd * 100) / 100).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
	     	if(data[0].percent_change_24h >= 0){
	        	$('#eth-percent').addClass('up');
	        	var percent = '<i class="fa fa-caret-up"></i> ' + data[0].percent_change_24h + '%';
	        }else{
	        	$('#eth-percent').addClass('down');
	        	var percent = '<i class="fa fa-caret-down"></i> ' + data[0].percent_change_24h + '%';
	        }
	        $('#eth-price').html(price);
	        $('#eth-percent').html(percent);
	    }
	});
	$.ajax({
	    url: "https://api.coinmarketcap.com/v1/ticker/ripple/",
	    dataType: 'json',
	    success: function(data) {
	        var price = '$ '+ (Math.round(data[0].price_usd * 100) / 100).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
	        if(data[0].percent_change_24h >= 0){
	        	$('#xrp-percent').addClass('up');
	        	var percent = '<i class="fa fa-caret-up"></i> ' + data[0].percent_change_24h + '%';
	        }else{
	        	$('#xrp-percent').addClass('down');
	        	var percent = '<i class="fa fa-caret-down"></i> ' + data[0].percent_change_24h + '%';
	        }
	        $('#xrp-price').html(price);
	        $('#xrp-percent').html(percent);
	    }
	});
	$.ajax({
	    url: "https://api.coinmarketcap.com/v1/ticker/litecoin/",
	    dataType: 'json',
	    success: function(data) {
	        var price = '$ '+ (Math.round(data[0].price_usd * 100) / 100).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
	        if(data[0].percent_change_24h >= 0){
	        	$('#ltc-percent').addClass('up');
	        	var percent = '<i class="fa fa-caret-up"></i> ' + data[0].percent_change_24h + '%';
	        }else{
	        	$('#ltc-percent').addClass('down');
	        	var percent = '<i class="fa fa-caret-down"></i> ' + data[0].percent_change_24h + '%';
	        }
	        $('#ltc-price').html(price);
	        $('#ltc-percent').html(percent);
	    }
	});
}
// coinmarketcap API

// sticky menu
$(window).on('load',function(){
	var sticky = $('#menu').position().top;
	revealOnScroll();

$(window).on('scroll',function(){
	if($(window).width() > 1250){
		scroll();
	}
	// revealOnScroll();
})

function scroll(){
	var param = sticky;
	var comp = (parseInt(param));
	var h = $('#menu').outerHeight();

	if ($(window).scrollTop() >= comp) {
		$('#menu').addClass("sticky");
		$('#section-one').css('margin-top',h); 
	} else {
		$('#menu').removeClass("sticky");
		$('#section-one').css('margin-top','0');
	}
}

 function revealOnScroll() {
    $(".article-item:not(.animated)").each(function () {
    	var h = $(this).offset().top - 200;
    	// console.log(h);
	    if($(window).scrollTop() > h){
	    	$(this).css('visibility','visible');
	    	$(this).addClass('animated zoomIn');
	    }
    });
}

})
//sticky menu

//socmed margin function
function socmed_margin(width){
	$('.socmed-item.show2').removeClass('mg-left0');
	$('.socmed-item.show2').addClass('mg-si');

	$('.socmed-item.show2').eq(0).removeClass('mg-si');
	$('.socmed-item.show2').eq(0).addClass('mg-left0');

	if(width <= 1260 && width > 820){
		var child = $('.socmed-item.show2').length;
		var param = Math.floor(child / 2);
		for(x=0;x < param;x++){
			cal = 2*x+2;
			console.log(cal);
			$('.socmed-item.show2').eq(cal).removeClass('mg-si');
			$('.socmed-item.show2').eq(cal).addClass('mg-left0');
		}
	}else if(width > 1260){
		var child = $('.socmed-item.show2').length;
		var param = Math.floor(child / 3);
		for(x=0;x < param;x++){
			cal = 3*x+3;
			console.log(cal);
			$('.socmed-item.show2').eq(cal).removeClass('mg-si');
			$('.socmed-item.show2').eq(cal).addClass('mg-left0');
		}
	}
}
//socmed margin function

$(window).resize(function(){

	//socmed load margin
	var width = $(window).width();
	socmed_margin(width);
	//socmed load margin	

})

//show more function
function showmore(){
	var data = '<div class="article-item"><div class="article-img" style="background-image: url(assets/img/test.jpg);"><div class="taggroup"><div class="btn btn-technology tag">TECHNOLOGY</div></div></div><div class="article-group"><div class="article-title">Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt.</div><div class="article-detail"><div class="article-date"><i class="fa fa-clock-o"></i>22 Dec 2017</div><div class="article-author"><i class="fa fa-user"></i>by John Alexa</div><div class="article-comment"><i class="fa fa-comments-o"></i>2,714 comments</div><div class="article-view"><i class="fa fa-eye"></i>28,741 views</div></div><div class="article-content">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.</div><div class="article-link"><div class="article-linkdet"><a href="">More:</a><div>John Doue and Michele Alexander G.</div></div><div class="article-linkdet"><a href="">Facebook:</a><div>Gregg Jrafee, Axios and Wasington Post</div></div><div class="article-linkdet"><a href="">Twitter:</a><div>@lizzye @ssamuel @adamentos @ericchen @alexaxaa @holynews @paperupdate @dailypost @reginaevly</div></div></div></div></div>';
	$("#section-two").append(data+data+data+data);
}
//show more function

function modalfunction(){
	
}